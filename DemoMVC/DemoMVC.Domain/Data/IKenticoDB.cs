﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMVC.Domain.Data
{
    public interface IKenticoDB :IDisposable
    {
        IQueryable<T> Query<T>() where T : class;

        void Insert<T>(T formData) where T : class;
    }
}
