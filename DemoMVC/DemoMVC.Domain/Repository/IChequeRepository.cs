﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoMVC.Domain.Data;

namespace DemoMVC.Domain.Repository
{
    public interface IChequeRepository
    {
        
        IList<custom_cheque> GetCheques();
        custom_cheque GetCheque(int chequeId);
        

    }
}
