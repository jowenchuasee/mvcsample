﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoMVC.Domain.Data;

namespace DemoMVC.Domain.Repository
{
    public class SimpleChequeRepository : IChequeRepository
    {
        Kentico10AppEntities _entities;

        public SimpleChequeRepository()
        {
            _entities = new Kentico10AppEntities();
        }

        public IList<custom_cheque> GetCheques()
        {
            return (from cheque in _entities.custom_cheque
                    orderby cheque.ChequeDate descending
                    select cheque).ToList();
        }

        public custom_cheque GetCheque(int chequeId)
        {
            return (from cheque in _entities.custom_cheque
                    where cheque.ChequeID == chequeId
                    orderby cheque.ChequeDate descending
                    select cheque).FirstOrDefault();
        }

        public void SubmitContactUs(Form_DemoSite_ContactUs formData)
        {
            _entities.Form_DemoSite_ContactUs.Add(formData);
            _entities.SaveChanges();
        }
    }
}
