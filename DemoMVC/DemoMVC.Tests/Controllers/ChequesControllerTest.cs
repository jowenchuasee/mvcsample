﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DemoMVC.Tests.Fakes;
using DemoMVC.Domain.Data;
using DemoMVC.Controllers;
using System.Web.Mvc;

namespace DemoMVC.Tests.Controllers
{
    /// <summary>
    /// Summary description for ChequeControllerTest
    /// </summary>
    [TestClass]
    public class ChequesControllerTest
    {
        [TestMethod]
        public void ChequeIndexTest()
        {
            FakeKenticoDB kenticoDB = new FakeKenticoDB();

            kenticoDB.AddSet<custom_cheque>(TestData.Cheques);

            ChequesController controller = new ChequesController(kenticoDB);

            ViewResult result = controller.Index() as ViewResult;
            IList<custom_cheque> cheques = result.Model as IList<custom_cheque>;

            Assert.AreEqual(2, cheques.Count);
        }
    }
}
