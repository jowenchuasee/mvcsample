﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.ComponentModel;
using DemoMVC.Domain.Repository;
using DemoMVC.Domain.Data;

namespace DemoMVC.Tests.Moq
{
    /// <summary>
    /// Summary description for MoqUnitTest
    /// </summary>
    [TestClass]
    public class MoqUnitTest
    {
        IChequeRepository _simpleChequeRepository;

        public MoqUnitTest()
        {
            Arrange();

        }

        private void Arrange()
        {
            IList<custom_cheque> cheques = new List<custom_cheque>()
            {
                new custom_cheque { ChequeID=1, ChequeAmount=100, ChequeDate=DateTime.Now, ChequeName="Person A" },
                new custom_cheque { ChequeID=2, ChequeAmount=200, ChequeDate=DateTime.Now, ChequeName="Person B" },
                new custom_cheque { ChequeID=3, ChequeAmount=300, ChequeDate=DateTime.Now, ChequeName="Person C" },
            };

            var mockChequeRepository = new Mock<IChequeRepository>();

            mockChequeRepository.Setup(a => a.GetCheques()).Returns(cheques);

            mockChequeRepository.Setup(a => a.GetCheque(It.IsAny<int>())).Returns((int i) => cheques.Where(a => a.ChequeID == i).FirstOrDefault());
            
            _simpleChequeRepository = mockChequeRepository.Object;
            
        }

        [TestMethod]
        public void SimpleRepositoryTest_GetSingleCheque()
        {
            custom_cheque cheque = _simpleChequeRepository.GetCheque(1);

            //Assert
            Assert.IsTrue(cheque != null);
            Assert.AreEqual("Person A", cheque.ChequeName);
        }
    }
}
