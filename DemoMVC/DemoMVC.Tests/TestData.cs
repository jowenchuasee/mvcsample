﻿using DemoMVC.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMVC.Tests
{
    public class TestData
    {
        public static IQueryable<custom_cheque> Cheques
        {
            get
            {
                List<custom_cheque> cheques = new List<custom_cheque>();

                cheques.Add(new custom_cheque { ChequeName = "Gordon Hayward", ChequeDate = DateTime.Now, ChequeAmount = 31000000, ChequeID = 1 });
                cheques.Add(new custom_cheque { ChequeName = "Isaiah Thomas", ChequeDate = DateTime.Now.AddDays(-1), ChequeAmount = 6000000, ChequeID = 2 });

                return cheques.AsQueryable();
            }
        } 
    }
}
