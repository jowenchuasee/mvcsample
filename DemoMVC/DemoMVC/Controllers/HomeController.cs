﻿using DemoMVC.Domain.Data;
using DemoMVC.Domain.Repository;
using DemoMVC.Domain.Service;
using DemoMVC.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DemoMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public void PostForm(Form_DemoSite_ContactUs formData)
        {
            ContactUsService svc = new ContactUsService();

            formData.FormInserted = DateTime.Now;
            formData.FormUpdated = DateTime.Now;
            svc.SubmitContactUs(formData);
            
            Response.Redirect("~/home/contact");
        }
    }
}