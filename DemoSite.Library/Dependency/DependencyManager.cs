﻿using DemoMVC.Domain.Repository;
using DemoSite.Library.Services;
using DemoSite.Library.Utilities;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSite.Library.Dependency
{
    public class DependencyManager
    {
        public static void RegisterDependencies()
        {
            UnityContainer _container = GlobalUnityContainer.Container;

            _container.RegisterType<INumberToTextService, NumberToTextService>();
            _container.RegisterType<INumberConverter, NumberConverter>();
            _container.RegisterType<IChequeRepository, SimpleChequeRepository>();
        }
    }
}
