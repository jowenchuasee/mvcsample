﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoSite.Library.Services;
using DemoSite.Library.Utilities;
using Microsoft.Practices.Unity;
using DemoSite.Library.Dependency;
using DemoMVC.Domain.Model;

namespace DemoSite.Library.Helpers
{
    public class TextConverterHelper
    {
        public static string ConvertToDollarText(double amount)
        {
            INumberConverter numberConverter = new NumberConverter();

            var numberToTextService = new NumberToTextService(numberConverter);

            return numberToTextService.GenerateText(amount);
        }
    }
}
