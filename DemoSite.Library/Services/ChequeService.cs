﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoMVC.Domain.Repository;
using DemoMVC.Domain.Model;

namespace DemoSite.Library.Services
{
    public class ChequeService
    {
        INumberToTextService _numberToTextService;
        IChequeRepository _chequeRepository;

        public ChequeService(IChequeRepository chequeRepository, INumberToTextService numberToTextService)
        {
            _numberToTextService = numberToTextService;
            _chequeRepository = chequeRepository;
        }

        public Cheque GetChequeDetail(int id)
        {
            var cheque = _chequeRepository.GetCheque(id);

            if (cheque != null)
            {
                return new Cheque()
                {
                    ChequeDetail = cheque,
                    ChequeText = _numberToTextService.GenerateText(cheque.ChequeAmount)
                };
            }

            return null;
        }
    }
}
