﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSite.Library.Utilities
{
    public interface INumberConverter
    {
        string ConvertNumber(string paramNumberString);
    }
}
