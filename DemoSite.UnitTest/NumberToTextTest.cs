﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DemoSite.Library.Services;
using DemoSite.Library.Utilities;
using System.Diagnostics;

namespace DemoSite.UnitTest
{
    [TestClass]
    public class NumberToTextTest
    {
        [TestMethod]
        public void Service_WholeNumber()
        {
            INumberConverter numberConverter = new NumberConverter();
            var service = new NumberToTextService(numberConverter);
            Assert.AreEqual(service.GenerateText(1111), "one thousand one hundred eleven dollars");
            
        }

        [TestMethod]
        public void Service_WithDecimal()
        {
            INumberConverter numberConverter = new NumberConverter();
            var service = new NumberToTextService(numberConverter);
            Debug.WriteLine(service.GenerateText(298.33));
            Assert.AreEqual(service.GenerateText(298.33), "two hundred ninety-eight dollars and thirty-three cents");
            
        }

        [TestMethod]
        public void Service_DollarOnly()
        {
            INumberConverter numberConverter = new NumberConverter();
            var service = new NumberToTextService(numberConverter);
            Assert.AreEqual(service.GenerateText(21298), "twenty-one thousand two hundred ninety-eight dollars");
            
        }

        [TestMethod]
        public void Service_CentsOnly()
        {
            INumberConverter numberConverter = new NumberConverter();
            var service = new NumberToTextService(numberConverter);
            Assert.AreEqual(service.GenerateText(0.55), "fifty-five cents");
            
        }

        [TestMethod]
        public void Service_OneCent()
        {
            INumberConverter numberConverter = new NumberConverter();
            var service = new NumberToTextService(numberConverter);
            Assert.AreEqual(service.GenerateText(169.01), "one hundred sixty-nine dollars and one cent");
            
        }

        [TestMethod]
        public void Service_Million()
        {
            INumberConverter numberConverter = new NumberConverter();
            var service = new NumberToTextService(numberConverter);
            Assert.AreEqual(service.GenerateText(169000000.01), "one hundred sixty-nine million dollars and one cent");
        }
    }
}
